<?php 

/* Página de opciones del theme
–––––––––––––––––––––––––––––––––––––––––––––––––– */

if( function_exists( 'acf_add_options_page' ) ){
	
	// Creamos la página de opciones

	acf_add_options_page(array(
		'page_title' 	=> 'Configuración inicial del tema',
		'menu_title'	=> 'byAdr Theme',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
