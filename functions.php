<?php

/* Definimos el Content Width
–––––––––––––––––––––––––––––––––––––––––––––––––– */

// En este caso es orientativo, ya que es una diseño flexible

if ( ! isset( $content_width ) ) {
	$content_width = 960;
}


/* Setup inicial
–––––––––––––––––––––––––––––––––––––––––––––––––– */
if ( ! function_exists( 'byadr_theme_setup' ) ) :

	function byadr_theme_setup(){
		// Habilitamos la traduccion del tema

		load_theme_textdomain( 'byadr', get_template_directory() . '/languages' );


		// Compatibilidad HTML5 
		
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		
		// Post Thumbnails
		add_theme_support( 'post-thumbnails' ); 

		// Custom Header
		$defaults = array(
			'default-image'          => get_template_directory_uri() . '/images/bghome.jpg',
			'random-default'         => false,
			'width'                  => 0,
			'height'                 => 0,
			'flex-height'            => true,
			'flex-width'             => false,
			'default-text-color'     => '',
			'header-text'            => false,
			'uploads'                => true,
			'wp-head-callback'       => '',
			'admin-head-callback'    => '',
			'admin-preview-callback' => '',
		);

		add_theme_support( 'custom-header', $defaults );

		// Registramos los menús

		register_nav_menus(
		    array(
		      'main-menu'	=>	__( 'Main Menu', 'byadr' ),
		      'footer-menu'	=>	__( 'Footer Menu', 'byadr'  ),
		      'social-menu' =>	__( 'Social Menu', 'byadr' )

		    )
		);

	}

endif; // byadr_theme_setup()
	
add_action( 'after_setup_theme', 'byadr_theme_setup' );


/* Widget Area
–––––––––––––––––––––––––––––––––––––––––––––––––– */

if ( ! function_exists( 'byadr_widgets_areas' ) ) :

	function byadr_widgets_areas(){
		
		register_sidebar( 
			array(
				'id'            => 'footer-widget',
				'name'          => __('Widget del footer','byadr'),
				'description'   => __('Widget del footer para la dirección e información de contacto','byadr'),
		        'class'         => '',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '<h5>',
				'after_title'   => '</h5>' 
			) 
		);


	}

endif;

add_action( 'widgets_init', 'byadr_widgets_areas' );


/* Encolamos estilos y scrips
–––––––––––––––––––––––––––––––––––––––––––––––––– */
if ( ! function_exists( 'byadr_set_styles_js' ) ) :

	function byadr_set_styles_js() {
		// Hoja estilo principal
	    wp_enqueue_style( 'main-style', get_stylesheet_uri() );

	    if ( !is_admin() ):
	    	// LLamamos a jQuery
			wp_enqueue_script('jquery');

			// Insertamos script Principal
			wp_register_script( 'main-script',  get_bloginfo('template_directory') . '/js/min/main.min.js', false, null, true);
			wp_enqueue_script('main-script');

	    endif;

	}

endif;

add_action( 'wp_enqueue_scripts', 'byadr_set_styles_js' );


/* Extras
–––––––––––––––––––––––––––––––––––––––––––––––––– */

include_once('inc/options.php');



/* Favicons & Apple Touch icons
–––––––––––––––––––––––––––––––––––––––––––––––––– 

if ( ! function_exists( 'byadr_add_favicons' ) ) :

	function byadr_add_favicons(){
		?>
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.ico">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.png">
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/iPad-icon.png">
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/iPadRetina-icon.png">
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/iPhone-icon.png">
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/iPhoneRetina-icon.png">
		<?php
	}

endif;

add_action( 'wp_enqueue_scripts', 'byadr_add_favicons' );
*/
/* longitud excerpt
–––––––––––––––––––––––––––––––––––––––––––––––––– 

if ( ! function_exists( 'byadr_excerpt_length' ) ) :

	function byadr_excerpt_length( $length ) {
		return 20;
	}

endif;

add_filter( 'excerpt_length', 'byadr_excerpt_length', 999 );
*/
/* Eliminamos estilos defecto galeria
–––––––––––––––––––––––––––––––––––––––––––––––––– */

add_filter( 'use_default_gallery_style', '__return_false' );


